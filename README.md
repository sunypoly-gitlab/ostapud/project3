# project3

Part 1 - Definition
--- Continuous Integration is an automation process for developers. When used correctly, CI allows code changes to be regularly built, tested and merged to a repository. CI provides a solution to having too many branches which may conflict with each other. According to the slides provided in this class, “Continuous Integration (CI) is the process of automating the build and testing procedures of a codebase such that both are performed every time a change is committed to the version control system (VCS). CI is used to verify the quality and stability of a new feature or bug fix automatically, potentially identifying new problems in the process.”

--- Continuous Delivery is a process which takes changes made to code, bug tests them and uploads it into the repository. The code is then deployed to the public by the operations team. According to the slides provided in class, “Continuous Delivery (CD) is the process of automating the release of an application so that: “A new release-ready build is available and can be easily deployed to a production environment upon approval.  The release candidate is automatically deployed to a test environment for UI, integration, load, and API testing.  It may be easily deployed to a production system.”

--- Pipelines consist of a series of steps set in place in order to deliver an updated version of software. Approaches such as DevOps and SRE are used to do so. According to the slides provided in class, “Pipelines are: A series of steps, called "stages", that describe all CI/CD operations  They are Fully automated and massively configurable (Variables, Logic Conditions, etc.)”

--- As mentioned in the pipeline definition, there are a series of steps that have to be cleared before an updated version of software is released, and those steps are referred to as stages. The slides provided in class mention six stages: prepare, build, lint, test, containerize and notify.

--- Jobs simply define what to do. Two examples provided by Gitlab include jobs that compile and jobs that test code. According to the slides provided in class, “Jobs are a sequence of COMMAND LINE instructions that when performed in the proper environment (read: container), should result in a successful operation assuming the codebase has not created any new issues”


Part 2 - Instruction/Code Snippets
a,b.) ex./.gitlab-ci.yml
________________
stages:
  -bld
  -tst
  -dply

bld_1:
  stage:bld
  script: echo “ ”

bld_2:
  stage:bld
  script: echo “ ”

tst_1:
  stage:tst
  script: echo “ ”

tst_2:
  stage:tst
  script: echo “ ”

dply_1:
  stage:dply
  script: echo “ ”

dply_2:
  stage:dply
  script: echo “ ”



e.) Only CI YAML completed
_______________
variables:
  TEST_VAR: "This is a global variable."

job1:
  variables:
    TEST_VAR_JOB: "This is a local variable, therefore only job1 can use it."
  script:
    - echo "$TEST_VAR" and "$TEST_VAR_JOB"

Part 3 - Questions
1.)Continuous Deployment contains all of the features of Continuous Delivery, however it has an, “automatic deployment to a production environment.” (from slides)

2.)Project settings are one location where GitLab CI variables can be set.

3.)Variables defined in CI YAML have precedence over those defined in runtime.

4.)We do have the option to source CI files from other locations including other repositories.

5.)Cache is used for dependencies such as downloads from the internet, whereas artifacts are used to pass build results between stages.

6.)Masked variables are simply variables that contain secrets such as values that aren’t supposed to be exposed to the end user.

7.)Protected variables are available only when the pipeline runs on a protected branch.

8.)According to documentation on the GitLab website, “The image keyword is the name of the Docker image the Docker executor uses to run CI/CD jobs,” whereas “The 
services keyword defines a Docker image that runs during a job linked to the Docker image that the image keyword defines.”

9.)The after_script command is useful because it can override a set of commands that are executed after a job.

10.)The default section configures all jobs in one section unless that job overrides this section.
